<?php

namespace App;

use App\Models\Content\Constants;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    const MALE = 0;
    const FEMALE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','lastname','age','gender','interests','city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getGenderAttribute($field){
        return $field === self::MALE ? __('Man') : __('Woman');
    }

    //SQL

    public function countSearchPages($value = null)
    {

        $where = '';
        if ($value !== null)
        {
            $where = " where name like '$value%' or lastname like '$value%'";
        }

        $request_str = "select count(*) from users {$where}";
        return DB::connection($this->connection)->select($request_str);
    }

    public function showSearchPagesWithLimit(int $page, int $limit, $value = null)
    {
        $where = '';
        $page *= $limit;

        if ($value !== null)
        {
            $where = " where name like '$value%' or lastname like '$value%' ";
        }

        $where .= " order by id desc limit {$page},{$limit}";
        $request_str = "select id, name, lastname, age, city, gender from users {$where}";
        return DB::connection($this->connection)->select($request_str);
    }

    public function showSearchPages(string $value = null)
    {
        $where = '';

        if ($value !== null)
        {
            $where = " where name like '$value%' or lastname like '$value%' ";
        }

        $request_str = "select * from users {$where}  order by id desc";
        return DB::connection($this->connection)->select($request_str);
    }

    public function findFirst($id)
    {
        $request_str = "select * from users where id = $id limit 1";
        $data = DB::connection($this->connection)->select($request_str);
        return $data[0] ?? null;
    }
}
