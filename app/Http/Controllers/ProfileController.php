<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Show profile.
     *
     * @param  $id
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $id = Auth::id();
        $model = new User();
        $user = $model->findFirst($id);
        $class = '';
        return view('profile.show', ['title' => __('Profile'), 'user' => $user, 'class' => $class,]);
    }

    /**
     * Show list profiles.
     *
     * @param Request $request
     * @param  int  $page
     */
    public function all(Request $request, int $page = 0)
    {
        $limit = 1000;
        $model = new User();
        $searchValue = $request->get('search');

        $count = $model->countSearchPages($searchValue);

        $pageLimit = $page * $limit;

        if ($pageLimit > $count) {
            return abort(404);
        }

        $users = $model->showSearchPagesWithLimit($page, $limit, $searchValue);

        if (Auth::check()) {
            $class = '';
        } else {
            $class = 'off-canvas-sidebar';
        }

        $params = [
            'title' => __('Profiles List'),
            'users' => $users,
            'class' => $class,
            'links' => [
                'prev' => $pageLimit !== 0 ? $page - 1 : null,
                'next' => $pageLimit + 1 <= $count ? $page + 1 : null
            ],
            'search' => $searchValue
        ];

        return view('profile.list', $params);
    }

    /**
     * Show select profile.
     *
     * @param  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = new User();
        $user = $model->findFirst($id);

        if (Auth::check()) {
            $class = '';
        } else {
            $class = 'off-canvas-sidebar';
        }

        return view('profile.show', ['title' => __('Profile'), 'user' => $user, 'class' => $class]);
    }

    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('profile.edit');
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());

        return back()->withStatus(__('Profile successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withStatusPassword(__('Password successfully updated.'));
    }
}
