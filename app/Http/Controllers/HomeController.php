<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show homepage.
     *
     */
    public function homepage()
    {
        if (Auth::check()) {
            return redirect('/home');
        }
        return view('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $this->middleware('auth');
        return view('dashboard');
    }
}
