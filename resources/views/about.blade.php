@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'about', 'title' => __('Mountain lovers')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-7 col-md-8">
          <h1 class="text-white text-center">{{ __('About') }}</h1>
          <h2 class="text-white text-center">{{ __('We are a progressive team. We love the mountains. We love to travel.') }}</h2>
          <br><br><br>
          <h2 class="text-white text-center">{{__('We have a small but very diligent team)')}}</h2>
      </div>
  </div>
</div>
@endsection
