@extends('layouts.app', ['class' => $class, 'activePage' => 'profiles', 'titlePage' => $title])

@section('content')
<div class="@auth() content @endauth @guest() container @endguest">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">{{__('Profile: ') . $user->name . $user->lastname}}</h4>
            <p class="card-category"> {{__('age: ') . $user->age}}</p>
            <p class="card-category"> {{__('city: ') . $user->city}}</p>
            <p class="card-category"> {{__('gender: ') . $user->gender}}</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    {{__('Email')}}
                  </th>
                  <th>
                    {{__('Interests')}}
                  </th>
                  <th>
                    {{ __('Creation date') }}
                  </th>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      {{ $user->email }}
                    </td>
                    <td>
                      {{ $user->interests }}
                    </td>
                    <td>
                      {{ date('Y-m-d', strtotime($user->created_at)) }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection