@extends('layouts.app', ['activePage' => 'profile', 'titlePage' => __('User Profile')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('profile.update') }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Edit Profile') }}</h4>
                <p class="card-category">{{ __('User information') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('First name') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('First name') }}" value="{{ old('name', auth()->user()->name) }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                  <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Last name') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('lastname') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" id="input-name" type="text" placeholder="{{ __('Last name') }}" value="{{ old('lastname', auth()->user()->lastname) }}" required="true" aria-required="true"/>
                      @if ($errors->has('lastname'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('lastname') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                  <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Age') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('age') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" name="age" id="input-name" type="text" placeholder="{{ __('Age') }}" value="{{ old('age', auth()->user()->age) }}" required="true" aria-required="true"/>
                      @if ($errors->has('age'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('age') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('Email') }}" value="{{ old('email', auth()->user()->email) }}" required />
                      @if ($errors->has('email'))
                        <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Interests') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('interests') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('interests') ? ' is-invalid' : '' }}" name="interests" id="input-name" type="text" placeholder="{{ __('Interests') }}" value="{{ old('interests', auth()->user()->interests) }}" required="true" aria-required="true"/>
                      @if ($errors->has('interests'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('interests') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('City') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" id="input-name" type="text" placeholder="{{ __('City') }}" value="{{ old('city', auth()->user()->city) }}" required="true" aria-required="true"/>
                      @if ($errors->has('city'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('city') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Gender') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('gender') ? ' has-danger' : '' }}">
                      <div class="form-check form-check-radio form-check-inline">
                          <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="gender" id="genderM" required value="0" @if(old('gender') == 0) checked @endif>
                            {{ __('man') }}
                            <span class="circle">
                                  <span class="check"></span>
                              </span>
                          </label>
                      </div>
                      <div class="form-check form-check-radio form-check-inline">
                          <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="gender" id="genderW" required value="1" @if(old('gender') == 1) checked @endif>
                            {{ __('woman') }}
                            <span class="circle">
                                  <span class="check"></span>
                              </span>
                          </label>
                      </div>
                      @if ($errors->has('gender'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('gender') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Age') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('age') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" name="age" id="input-name" type="text" placeholder="{{ __('Age') }}" value="{{ old('name', auth()->user()->age) }}" required="true" aria-required="true"/>
                      @if ($errors->has('age'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('age') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('profile.password') }}" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Change password') }}</h4>
                <p class="card-category">{{ __('Password') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status_password'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status_password') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-current-password">{{ __('Current Password') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" input type="password" name="old_password" id="input-current-password" placeholder="{{ __('Current Password') }}" value="" required />
                      @if ($errors->has('old_password'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('old_password') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-password">{{ __('New Password') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="input-password" type="password" placeholder="{{ __('New Password') }}" value="" required />
                      @if ($errors->has('password'))
                        <span id="password-error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-password-confirmation">{{ __('Confirm New Password') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="{{ __('Confirm New Password') }}" value="" required />
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Change password') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection