@extends('layouts.app', ['class' => $class, 'activePage' => 'profiles', 'titlePage' => $title])

@section('content')
  <div class="@auth() content @endauth @guest() container @endguest">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">{{__('Profiles')}}</h4>
            @if($search !== null)
              <p class="card-category">
                {{__('You search: ') . $search}}
              </p>
            @endif
          </div>
          <div class="card-body">
            <form method="get" action="{{ route('profiles') }}" autocomplete="off" class="form-horizontal">
              <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Search') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control{{ $errors->has('search') ? ' is-invalid' : '' }}" name="search" id="input-search" type="text" placeholder="{{ __('firstname or lastname') }}" value="{{ ($search !== null) ? $search : '' }}"/>
                    </div>
                  </div>
                </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-default">{{ __('Search') }}</button>
              </div>
              </form>
            <hr>
            @include('profile.table', ['links' => $links, 'users' => $users])
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection