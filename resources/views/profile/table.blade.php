<div class="table-responsive">
              @include('profile.links', ['links' => $links])
  <table class="table">
                <thead class=" text-primary">
                  <th>
                    {{__('Full name')}}
                  </th>
                  <th>
                    {{__('Age')}}
                  </th>
                  <th>
                    {{__('City')}}
                  </th>
                  <th>
                    {{__('Gender')}}
                  </th>
                </thead>
                <tbody>
                @foreach($users as $user)
                  <tr>
                    <td>
                      <a href = "{{route('profile.show', ['id' => $user->id])}}" target="_blank">{{ $user->name . ' ' . $user->lastname }}</a>
                    </td>
                    <td>
                      {{ $user->age }}
                    </td>
                    <td>
                      {{ $user->city }}
                    </td>
                    <td>
                      {{ $user->gender === 0 ? __('M') : __('W') }}
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
              <br>
  @include('profile.links', ['links' => $links])
            </div>