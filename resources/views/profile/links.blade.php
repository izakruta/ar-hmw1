@if(isset($links))
  @if(isset($links['prev']) && $links['prev'] !== null)
    <a class="btn btn-default" href = "{{route('profiles', ['page' => $links['prev']])}}">{{__('prev page')}}</a>
  @endif
  @if(isset($links['next']) && $links['next'] !== null)
    <a class="btn btn-primary" href = "{{route('profiles', ['page' => $links['next']])}}">{{__('next page')}}</a>
  @endif
@endif