<footer class="footer">
    <div class="container">
        <nav class="float-left">
        <ul>
            <li>
            <a href="{{route('about')}}">
                {{ __('About Us') }}
            </a>
            </li>
        </ul>
        </nav>
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>, made with <i class="material-icons">favorite</i> by Ilya Zakruta.
        </div>
    </div>
</footer>