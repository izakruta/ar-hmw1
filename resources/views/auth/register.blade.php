@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'register', 'title' => __('Mountain lovers')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
              <p class="card-title"><strong>{{ __('REGISTRATION') }}</strong></p>
            </div>
          <div class="card-body ">
            <div class="bmd-form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">face</i>
                  </span>
                </div>
                <input type="text" name="name" class="form-control" placeholder="{{ __('First name...') }}" value="{{ old('name') }}" required>
              </div>
              @if ($errors->has('name'))
                <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                  <strong>{{ $errors->first('name') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('lastname') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">face</i>
                  </span>
                </div>
                <input type="text" name="lastname" class="form-control" placeholder="{{ __('Last name...') }}" value="{{ old('lastname') }}" required>
              </div>
              @if ($errors->has('lastname'))
                <div id="name-error" class="error text-danger pl-3" for="lastname" style="display: block;">
                  <strong>{{ $errors->first('lastname') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('age') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">cake</i>
                  </span>
                </div>
                <input type="number" min="18" max="120" step="1" name="age" class="form-control" placeholder="{{ __('Age...') }}" value="{{ old('age') }}" required>
              </div>
              @if ($errors->has('age'))
                <div id="name-error" class="error text-danger pl-3" for="age" style="display: block;">
                  <strong>{{ $errors->first('age') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('gender') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">group</i>
                  </span>
                </div>
                <div class="form-check form-check-radio form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="gender" id="genderM" required value="0" @if(old('gender')) checked @endif>
                      {{ __('man') }}
                        <span class="circle">
                            <span class="check"></span>
                        </span>
                    </label>
                </div>
                <div class="form-check form-check-radio form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="gender" id="genderW" required value="1" @if(old('gender')) checked @endif>
                        {{ __('woman') }}
                        <span class="circle">
                            <span class="check"></span>
                        </span>
                    </label>
                </div>
              </div>
              @if ($errors->has('gender'))
                <div id="name-error" class="error text-danger pl-3" for="gender" style="display: block;">
                  <strong>{{ $errors->first('gender') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">room</i>
                  </span>
                </div>
                <input type="text" name="city" class="form-control" placeholder="{{ __('City...') }}" value="{{ old('city') }}" required>
              </div>
              @if ($errors->has('city'))
                <div id="name-error" class="error text-danger pl-3" for="city" style="display: block;">
                  <strong>{{ $errors->first('city') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('interests') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">favorite</i>
                  </span>
                </div>
                <input type="text" name="interests" class="form-control" placeholder="{{ __('Interests...') }}" value="{{ old('interests') }}" required>
              </div>
              @if ($errors->has('city'))
                <div id="name-error" class="error text-danger pl-3" for="interests" style="display: block;">
                  <strong>{{ $errors->first('interests') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" value="{{ old('email') }}" required>
              </div>
              @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password...') }}" required>
              </div>
              @if ($errors->has('password'))
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>{{ $errors->first('password') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirm Password...') }}" required>
              </div>
              @if ($errors->has('password_confirmation'))
                <div id="password_confirmation-error" class="error text-danger pl-3" for="password_confirmation" style="display: block;">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </div>
              @endif
            </div>
            <div class="form-check mr-auto ml-3 mt-3">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="policy" name="policy" {{ old('policy', 1) ? 'checked' : '' }} >
                <span class="form-check-sign">
                  <span class="check"></span>
                </span>
                {{ __('I agree with the ') }} <a href="{{route('policy')}}">{{ __('Privacy Policy') }}</a>
              </label>
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Create account') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
