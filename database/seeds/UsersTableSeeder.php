<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->insert([
//            'name' => 'Ilya',
//            'lastname' => 'Zakruta',
//            'age' => 30,
//            'city' => 'Minsk',
//            'gender' => 0,
//            'interests' => 'Mounts',
//            'email' => 'ilya@mountain-lovers.com',
//            'email_verified_at' => now(),
//            'password' => Hash::make('secret'),
//            'created_at' => now(),
//            'updated_at' => now()
//        ]);

        factory(App\User::class, 20000)->create();
    }
}
